import './Modal.scss'
// import ModalWrapper from './ModalWrapper'

function Modal({children, onClose}){
    return <div className='modal__conteiner' onClick={onClose}>
        {children}
        {/* <ModalWrapper /> */}

    </div>
}

export default Modal