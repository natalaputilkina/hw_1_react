import './ModalWrapper.scss'
// import ModalBody from './ModalBody'

function ModalWrapper ({children, onClose}){
    return <section className='modal-wrapper' onClick={onClose}>
        {children}
        {/* <ModalBody /> */}
    </section>

}
export default ModalWrapper