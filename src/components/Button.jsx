import './Button.scss'


function Button ({type = 'button', classNames, onClick, children, title}){


    // function clickButton (){
    //     console.log('Button')
    // }

    return (
            <button className={classNames} onClick={onClick} type={type}>
                {title}
                {children}
            </button>
      
   
    )
}

export default Button;