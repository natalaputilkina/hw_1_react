import { useState } from "react"
import Button from "./components/Button"
import ModalText from "./components/Modal/ModalText"
import ModalImage from "./components/Modal/ModalImage"
import Modal from "./components/Modal/Modal"
import ModalWrapper from "./components/Modal/ModalWrapper"
import ModalHeader from "./components/Modal/ModalHeader"
import ModalFooter from "./components/Modal/ModalFooter"
import ModalClose from "./components/Modal/ModalClose"
import ModalBody from "./components/Modal/ModalBody"



function App() {

  const [isOpenFirstModal, setIsOpenFirstModal] = useState(false)
  const [isOpenSecondModal, setIsOpenSecondModal] = useState(false)
  

  return (
    <>
      
      <Button classNames='button_modal' title="Open first modal" onClick={() => setIsOpenFirstModal(true)}></Button>
      
      <Button classNames='button_modal' title="Open second modal" onClick={() => setIsOpenSecondModal(true)}></Button>

      {isOpenFirstModal && <ModalWrapper onClose={() => setIsOpenFirstModal(false)}>
        
          <Modal>
            <ModalHeader>
              <ModalImage/>
            </ModalHeader>
            <ModalClose/>
            <ModalBody>
              <ModalText title = "Product Delete!" text="By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted."/>
            </ModalBody>
            <ModalFooter firstText="NO, CANCEL" secondaryText="YES, DELETE"/>
          </Modal>

        
      </ModalWrapper>}
      
      

      

      {isOpenSecondModal && <ModalWrapper onClose={() => setIsOpenSecondModal(false)}>
        
        <Modal>
          <ModalClose/>
          <ModalBody>
            <ModalText title = "Add Product “NAME”" text="Description for you product"/>
          </ModalBody>
          <ModalFooter firstText="ADD TO FAVORITE"/>
        </Modal>

      
    </ModalWrapper>}

      
       
    </>
  )
}

export default App
